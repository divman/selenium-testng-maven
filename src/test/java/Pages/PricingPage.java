package Pages;

import TestBase.BaseTest;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.time.Duration;

public class PricingPage extends BaseTest {

    private final WebDriverWait wait;

    @FindBy(how = How.CSS, using = "li.d-block:nth-child(6) > details:nth-child(1) > summary:nth-child(1)")
    private WebElement linkPricing;

    @FindBy(how = How.CSS, using = "p.h00-mktg")
    private WebElement txtPlanHeaderText;

    @FindBy(how = How.XPATH, using = "//a[@data-ga-click=\"(Logged out) Header, go to Pricing\"]")
    private WebElement linkPlans;


    public PricingPage() {
        PageFactory.initElements(driver, this);
        wait = new WebDriverWait(driver, Duration.ofSeconds(60));
    }

    public void clickPricing() throws Throwable {
        wait.until(ExpectedConditions.visibilityOf(linkPricing));
        Actions action = new Actions(driver);
        action.moveToElement(linkPricing);
        linkPricing.click();
    }

    public void clickPlans() throws Throwable {
        wait.until(ExpectedConditions.visibilityOf(linkPlans));
        linkPlans.click();
    }

    public String getPlanHeaderText() throws Throwable {
        wait.until(ExpectedConditions.visibilityOf(txtPlanHeaderText));
        return txtPlanHeaderText.getText();
    }
}
