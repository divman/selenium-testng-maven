package Pages;

import TestBase.BaseTest;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.time.Duration;

public class HomePage extends BaseTest {

    private final WebDriverWait wait;

    @FindBy(how = How.XPATH, using = "//a[@href=\"/login\"]")
    private WebElement linkSignIn;

    @FindBy(how = How.ID, using = "login_field")
    private WebElement inputUsername;

    @FindBy(how = How.ID, using = "password")
    private WebElement inputPassword;

    @FindBy(how = How.CSS, using = "input[name=\"commit\"]")
    private WebElement btnSignin;

    @FindBy(how = How.CSS, using = "div#js-flash-container > div > div")
    private WebElement txtErrorMessage;

    public HomePage() {
        PageFactory.initElements(driver, this);
        wait = new WebDriverWait(driver, Duration.ofSeconds(60));
    }

    public void clickSignin() {
        wait.until(ExpectedConditions.visibilityOf(linkSignIn));
        linkSignIn.click();
    }

    public void loginApp(String user, String pass) {
        wait.until(ExpectedConditions.visibilityOf(inputUsername));
        inputUsername.sendKeys(user);
        inputPassword.sendKeys(pass);
        btnSignin.click();
    }

    public String getErrorMessage() {
        wait.until(ExpectedConditions.visibilityOf(txtErrorMessage));
        return txtErrorMessage.getText();
    }

    /*
    public void clickSignin() {
        //clickOnElement(linkSignIn);
        wait.until(d -> ((JavascriptExecutor) d)).executeScript("return document.readyState !== 'loading'");
        JavascriptExecutor js = (JavascriptExecutor) driver;
        try {
            wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("(//a[contains(text(), \"Sign in\")])[1]")));
            js.executeScript("arguments[0].click();", linkSignIn);
        } catch (StaleElementReferenceException e) {
            wait.until(d -> ((JavascriptExecutor) d)).executeScript("return document.readyState !== 'loading'");
            wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("(//a[contains(text(), \"Sign in\")])[1]")));
            WebElement linkSignIn = driver.findElement(By.xpath("(//a[contains(text(), \"Sign in\")])[1]"));
            js.executeScript("arguments[0].click();", linkSignIn);
        }
    }

    private void clickOnElement(WebElement element) {
        try {
            waitForElementToBeClickableBy(element).click();
        } catch (StaleElementReferenceException e) {
            for (int attempts = 1; attempts < 100; attempts++) {
                try {
                    waitForElementToBeClickableBy(element).click();
                    break;
                } catch (StaleElementReferenceException e1) {
                    System.out.println("Stale element found retrying:" + attempts);
                }
            }
        }
    }

    private WebElement waitForElementToBeClickableBy(WebElement element) {
        WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(60));
        return wait.until(ExpectedConditions.elementToBeClickable(element));
    }
    */
}
