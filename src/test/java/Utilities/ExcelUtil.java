package Utilities;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;

public class ExcelUtil {

    public static ArrayList<HashMap<String, Object>> readFile(InputStream inputStream, String sheetName) throws IOException {

        ArrayList<HashMap<String, Object>> list = new ArrayList<>();
        XSSFWorkbook workbook = new XSSFWorkbook(inputStream);
        XSSFSheet sheet = workbook.getSheet(sheetName);

        Row headerRow = sheet.getRow(0);
        for (int i = 1; i <= sheet.getLastRowNum(); i++) {

            HashMap<String, Object> map = new HashMap<>();
            Row dataRow = sheet.getRow(i);

            if (dataRow.getCell(0).getStringCellValue().equalsIgnoreCase("YES")) {

                for (int j = 0; j < headerRow.getLastCellNum(); j++) {
                    String attributeName = headerRow.getCell(j).getStringCellValue();
                    Cell attributeValue = dataRow.getCell(j);

                    switch (dataRow.getCell(j).getCellType()) {

                        case STRING:
                            map.put(attributeName, attributeValue.getStringCellValue());
                            break;
                        case NUMERIC:
                            // do something with integer and Date values
                            map.put(attributeName, String.valueOf(attributeValue.getNumericCellValue()));
                            break;
                        case BOOLEAN:
                            map.put(attributeName, attributeValue.getBooleanCellValue());
                            break;
                        default:
                            break;
                    }
                }
                list.add(map);
            }
        }

        inputStream.close();
        workbook.close();
        System.out.println(list);
        return list;
    }
}
