package Utilities;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.util.Properties;

public class PropertyConfig {

    static Properties properties = new Properties();
    private static final String strFile = "src/test/resources/Properties/config.properties";

    public static void loadProperty() throws Exception {
        try (InputStream inputStream = new FileInputStream(strFile)) {
            properties.load(inputStream);
        } catch (Exception ex) {
            throw new Exception(ex.getMessage());
        }
    }

    public static String getProperty(String key) throws Exception {
        try {
            return properties.get(key).toString();
        } catch (Exception e) {
            throw new Exception("Value is not specified for key: " + key + " in properties file.");
        }
    }

    public void setProperty(String key, String value) throws Exception {
        try (FileInputStream inputStream = new FileInputStream(strFile)) {
            properties.load(inputStream);
            properties.put(key, value);
            inputStream.close();
            FileOutputStream output = new FileOutputStream(strFile);
            properties.store(output, "This is overwrite file - Do not delete the 'token' key from this property file.");
            output.close();
        } catch (Exception ex) {
            throw new Exception(ex.getMessage());
        }
    }
}
