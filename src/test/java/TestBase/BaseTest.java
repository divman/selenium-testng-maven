package TestBase;

import Utilities.ExcelUtil;
import Utilities.PropertyConfig;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.testng.annotations.DataProvider;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.TimeUnit;

public class BaseTest extends ObjectsRepo {

    public void selectBrowser() {

        String browser = System.getProperty("browser");

        if (browser.equalsIgnoreCase("chrome")) {
            System.setProperty("webdriver.chrome.driver", "src/test/resources/Drivers/chromedriver");
            driver = new ChromeDriver();
        } else if (browser.equalsIgnoreCase("ie")) {
            System.setProperty("webdriver.chrome.driver", "src/test/resources/Drivers/chromedriver");
            driver = new InternetExplorerDriver();
        } else {
            System.setProperty("webdriver.chrome.driver", "src/test/resources/Drivers/chromedriver");
            driver = new ChromeDriver();
        }

        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        driver.manage().window().maximize();
    }

    public void launchApplication() throws Exception {

        String env = System.getProperty("env");
        PropertyConfig.loadProperty();
        driver.get(PropertyConfig.getProperty(env));
    }

    @DataProvider(name = "dataFromExcel")
    public Iterator<Object[]> getSampleList() throws IOException {

        List<HashMap<String, Object>> dataList =
                ExcelUtil.readFile(this.getClass().getResourceAsStream("/TestDatas/sample_file.xlsx"), "HcodeSheet");
        ArrayList<Object[]> data = new ArrayList<>();
        for (HashMap<String, Object> t : dataList) {
            data.add(new Object[]{t});
        }

        return data.iterator();

        /*
        ArrayList<Object[]> list = new ArrayList<>();
        list.add(new Object[]{"one"});
        list.add(new Object[]{"two"});
        list.add(new Object[]{"three"});
        list.add(new Object[]{"four"});
        list.add(new Object[]{"five"});
        return list.iterator();
        */
    }
}
