package TestBase;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import org.openqa.selenium.WebDriver;

public class ObjectsRepo {

    public static WebDriver driver;
    public static ExtentReports extent;
    public static ExtentTest test;
}
