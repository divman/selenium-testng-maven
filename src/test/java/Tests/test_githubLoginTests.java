package Tests;

import Pages.HomePage;
import TestBase.BaseTest;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

/* execute command
mvn clean install -Dbrowser="chrome" -Denv="qa" -Dsurefire.suiteXmlFiles="pricing.xml"
 */

public class test_githubLoginTests extends BaseTest {

    @BeforeMethod   // it will get execute before each test method within current class
    public void setupMethod() throws Exception {
        selectBrowser();
        launchApplication();
    }

    @Test(groups = {"high"})
    public void samplePositiveTest() throws Throwable {
        HomePage homePage = new HomePage();
        homePage.clickSignin();
        homePage.loginApp("test@test.com", "test123");
        String errMsg = homePage.getErrorMessage();
        test.info("The actual text from screen is : " + errMsg);
        Assert.assertEquals(errMsg, "Incorrect username or password.");
    }

    @Test(groups = {"low"})
    public void sampleFailingTest() throws Throwable {
        HomePage homePage = new HomePage();
        homePage.clickSignin();
        homePage.loginApp("test@test.com", "test123");
        String errMsg = homePage.getErrorMessage();
        test.info("The actual text from screen is : " + errMsg);
        Assert.assertEquals(errMsg, "Success Login.");
    }

    @AfterMethod
    public void cleanUp() {
        driver.quit();
    }
}
