package Tests;

import Pages.PricingPage;
import TestBase.BaseTest;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

public class test_githubPricingTests extends BaseTest {

    @BeforeMethod   // it will get execute before each test method within current class
    public void setupMethod() throws Exception {
        selectBrowser();
        launchApplication();
    }

    @Test(groups = {"high"})
    public void samplePositiveTest() throws Throwable {
        PricingPage pricingPage = new PricingPage();
        pricingPage.clickPricing();
        pricingPage.clickPlans();
        String headerText = pricingPage.getPlanHeaderText();
        test.info("The actual text from screen is : " + headerText);
        Assert.assertEquals(headerText, "Plans for all developers");
    }

    @Test(groups = {"low"})
    public void sampleFailingTest() throws Throwable {
        PricingPage pricingPage = new PricingPage();
        pricingPage.clickPricing();
        pricingPage.clickPlans();
        String headerText = pricingPage.getPlanHeaderText();
        test.info("The actual text from screen is : " + headerText);
        Assert.assertEquals(headerText, "Do not choose any plan");
    }

    @AfterMethod
    public void cleanUp() {
        driver.quit();
    }
}
