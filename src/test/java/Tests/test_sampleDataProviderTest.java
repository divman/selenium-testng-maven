package Tests;

import TestBase.BaseTest;
import org.testng.annotations.Test;

import java.util.HashMap;

public class test_sampleDataProviderTest extends BaseTest {

    @Test(dataProvider = "dataFromExcel")
    public void dataProviderTest(HashMap<String, Object> map) {

        String testRun = (String) map.get("TestRun");
        String testName = (String) map.get("TestName");
        String hCode = (String) map.get("Hcode");

        System.out.println("The testRun value is : " + testRun);
        System.out.println("The testName value is : " + testName);
        System.out.println("The hCode value is : " + hCode);
    }

}
